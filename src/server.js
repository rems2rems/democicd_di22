import mongoose from "mongoose";
import Recipe from "./recipes/model.js";
import express from "express";

const app = express()
app.use(express.urlencoded({ extended: true }))
app.set("view engine", "ejs")
app.set("views", "./src/views")
app.get("/", async (req, res) => {
    const recipes = await Recipe.find().limit(10)
    console.log(recipes);
    res.render("index.ejs",{recipes})
})
app.get("/recipes/new", (req, res) => {
    res.render("recipes/add.ejs")
})
app.post("/recipes", async (req, res) => {
    const recipe = new Recipe(req.body)
    await recipe.save()
    res.redirect("/")
})
app.get("/recipes/:id/delete", async (req, res) => {
    await Recipe.findByIdAndDelete(req.params.id)
    res.redirect("/")
})

mongoose.connect(process.env.MONGODB_URI).then(() => {
    console.log("Connected to MongoDB")
    
    Recipe.find().then((recipes) => {
        if(recipes.length === 0){
            let recipe = new Recipe({
                name: "bolognaise",
                description: "a classic bolognese dish",
                ingredients: ["pasta", "tomatoes", "onions", "olives", "garlic", "butter", "salt", "pepper"],
                instructions: ["boil water", "add pasta", "add sauce", "serve"]
            })
            
            recipe.save().then(() => {
                console.log("Recipe saved")
            })
        }
    })
    
    eval("let test = 'danger!';")
    app.listen(3000, () => console.log("listening on port 3000"))
})