FROM node:22

ADD package.json /app/package.json

WORKDIR /app
RUN npm install
ADD . .

EXPOSE 3000

CMD ["npm", "start"]