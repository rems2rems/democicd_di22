import { test, expect } from '@playwright/test';
import dotenv from 'dotenv';
dotenv.config();
test('user can delete a recipe', async ({ page }) => {
  await page.goto(process.env.BASE_URL);
  let list = await page.getByRole('list')
  let recipes = list.getByText('[X]')
  const oldCount = await recipes.count()
  

  const deleteRecipes = await list.getByRole('link', { name: '[X]' })
  //get last element
  const lastRecipe = deleteRecipes.last()
  await lastRecipe.click()
  list = await page.getByRole('list')
  recipes = list.getByText('[X]')
  const newCount = await recipes.count()

  expect(newCount).toBe(oldCount - 1)
  
})