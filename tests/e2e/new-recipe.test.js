import { test, expect } from '@playwright/test';
import dotenv from 'dotenv';
try {
  dotenv.config({});
}
catch (err) {
  console.log(err);
}

test('user can add a new recipe', async ({ page }) => {
  const newRecipe = 'burger'

  await page.goto(process.env.BASE_URL);
  //count existing recipes
  let list = await page.getByRole('list')
  let recipes = await list.getByText('[X]')
  const oldCount = await recipes.count()

  await page.goto(process.env.BASE_URL + '/recipes/new');
  //expect page to show a form for adding a new recipe
  await expect(page.getByLabel('Name')).toBeVisible();
  await expect(page.getByLabel('Description')).toBeVisible();
  await expect(page.getByLabel('Ingredients')).toBeVisible();
  await expect(page.getByLabel('Instructions')).toBeVisible();

  //add a new recipe
  await page.getByLabel('Name').fill("burger")
  await page.getByLabel('Description').fill("a delicious burger")
  await page.getByLabel('Ingredients').fill("beef, lettuce, tomato")
  await page.getByLabel('Instructions').fill("cook, eat, enjoy")
  await page.locator('button[type="submit"]').click()

  // await page.goto(process.env.BASE_URL);
  //re-count recipes
  list = await page.getByRole('list')
  recipes = await list.getByText('[X]')
  const newCount = await recipes.count()

  expect(newCount).toBe(oldCount + 1)
})
