import { test, expect } from '@playwright/test';
import dotenv from 'dotenv';
dotenv.config();
test('homepage lists 10 recipes', async ({ page }) => {
  await page.goto(process.env.BASE_URL);
  const list = await page.getByRole('list')
  const recipes = list.getByRole('link')
  const nbRecipes = await recipes.count()
  expect(nbRecipes).toBeGreaterThan(0)
  expect(nbRecipes).toBeLessThanOrEqual(10)
})

test('homepage shows a "new recipe" button', async ({ page }) => {
  await page.goto(process.env.BASE_URL);
  const button = await page.getByRole('link', { name: 'Add a new recipe' })
  await expect(button).toBeVisible()
  await expect(await button.getAttribute("href")).toBe("/recipes/new")
})

